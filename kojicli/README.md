# Koji CLI docker image

This docker image contains the latest maintained version of the Koji CLI,
preconfigured to connect to https://koji.cern.ch.

If the environment variables `KOJICI_USER` and `KOJICI_PWD` are defined, the
image automatically obtains a token for that user.
