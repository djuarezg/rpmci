# Do a kinit if we have the ENV variables set
if [[ ! -z "${KOJICI_USER}" && ! -z "${KOJICI_PWD}" ]]; then
    echo -e "\e[32mGetting Kerberos token for ${KOJICI_USER}:\e[0m"
    echo "${KOJICI_PWD}" | kinit ${KOJICI_USER}@CERN.CH
    RET=$?
    if [[ $RET -ne 0 ]]; then
        echo -e "\e[1m\e[31mError while running kinit\e[0m"
        exit $RET
    fi
fi
