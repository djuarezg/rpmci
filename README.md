# CI integration for building RPMs

This repo exists to help you build RPMs using Koji and Gitlab's CI infrastructure. You have here an [example of how to use it](https://gitlab.cern.ch/linuxsupport/myrpm).


## Prerequirements:

1. A [Koji tag](https://cern.service-now.com/service-portal/report-ticket.do?name=Koji-tag&fe=IT-RPM) where your packages will end up.
2. A [service account](https://account.cern.ch/account/Management/NewAccount.aspx) with [access to Koji](https://cern.service-now.com/service-portal/report-ticket.do?name=request&fe=IT-RPM), and in particular to your Koji tag. We'll use the account `kojici` as an example, but you must have your own. This account can be used for as many RPMs as you want.
3. A [Gitlab](https://gitlab.cern.ch/) repository with Pipelines (ie. continuous integration) enabled. You can enable Pipelines by going to *General Settings* for your repo and toggling the *Pipelines* feature under *Visibility, project features, permissions*. More info in [KB0003690](https://cern.service-now.com/service-portal/article.do?n=KB0003690).
4. Source code with a `Makefile` (or a `Makefile.koji`, which will take precedence if both exist) that defines two mandatory targets, `srpm` and `rpm`. Those targets should build at least one source RPM and an RPM, respectively. Check our [RPM starter](https://gitlab.cern.ch/linuxsupport/myrpm) repo for a working example.

Note: RPMCI uses [interruptible](https://docs.gitlab.com/ee/ci/yaml/#interruptible) jobs in order to stop the execution of a pipeline if a newer pipeline has made it redundant. In order to take advantage of this feature, [Auto-cancel pending pipelines](https://docs.gitlab.com/ee/ci/pipelines/settings.html#auto-cancel-pending-pipelines) has to be enabled for your project.

## Procedure:

1. Create your new empty repo. *Protip: Create a subgroup for all the RPMs of your service/group, and create your new repo inside it.*
2. Copy **all** the files from our [RPM starter](https://gitlab.cern.ch/linuxsupport/myrpm) repo.
3. Edit the variables in `.gitlab-ci.yml` (see below) and fill in the name of your service account, your Koji tag, etc. **Don't modify the include!**
4. In Gitlab, go to *Settings > CI / CD > Variables*. Add two new variables called `KOJICI_USER` and `KOJICI_PWD` and set them to the username and password of your service account. *Protip: If you followed the protip in #1, you can set these variables at the subgroup level and all repos within it will be able to use them automatically!*
5. From *lxplus* or *aiadm*, manually add your package to your Koji tag. For example:
    ```bash
    ~ > koji add-pkg --owner=$USER mytag7-testing myrpm
    ~ > koji add-pkg --owner=$USER mytag7-qa myrpm
    ~ > koji add-pkg --owner=$USER mytag7-stable myrpm
    ```
6. Place all your code in the `src/` directory. Don't forget to update the version number and the changelog in your spec file!
7. Commit all your changes to your new repo.
8. Treat yourself to a coffee while Gitlab runs your new pipelines! A Koji scratch (ie. temporary) build will be created, allowing you to test if your build succeeds or not. If your build takes more than an hour, you may need to [increase the timeout](#ci-jobs-timing-out) for your Gitlab project.
9. Once you're satisfied with your RPM and the Gitlab CI pipelines are passing, you can [create a tag](https://git-scm.com/book/en/v2/Git-Basics-Tagging) in your git repo. When you create a tag, Gitlab will run the tests again and, if they pass, will then launch a (non-scratch, ie. permanent) build job in Koji. *Protip: use the version of the new RPM as the tag name, future you will thank you.*
10. When that build succeeds, you'll be able to manually promote it to QA of your Koji tag, and from there to Stable.


### Available Configuration Variables:

Here are the configuration options you can set in your `.gitlab-ci.yml` file, and their defaults:

```yaml
  BUILD_8: 'False'
  BUILD_7: 'False'
  BUILD_6: 'False'
  DIST_8: '.el8'
  DIST_7: '.el7'
  DIST_6: '.slc6'
  KOJI_TAG_8: ''
  KOJI_TAG_7: ''
  KOJI_TAG_6: ''
```

If your Koji tag is configured with a custom disttag (such as `.el7.cern`), make
sure you add it to the appropriate `DIST_*` variable.

Your Koji tag will usually be configured in the `KOJI_TAG` variable and it will automatically
get the OS' release appended to it (ie. `KOJI_TAG: ai` will become `ai7`, etc.).
If you need to configure a different tag for a particular OS, you can do so using the `KOJI_TAG_*`
variables, but you will have to specify the OS release yourself (ie. `KOJI_TAG_8: config8`):

```yaml
  KOJI_TAG: 'mytag'
  KOJI_TAG_8: 'mynewtag8'
  BUILD_8: 'True'
  BUILD_7: 'True'
  BUILD_6: 'True'
```

## Customizations

### Adding your own tests

This CI pipeline includes a `test` stage that is run after building the scratch RPM
on Koji. There's a "base" test job called `.test_install` that installs your RPMs
in the `before_script` section. This job is then extended by OS-specific jobs
(`test_install6`, `test_install7` or `test_install8`), which are the ones actually executed.

If you want to run extra actions (functional tests, etc.) once your RPMs are installed,
you can do so in the `script` section:

```yaml
.test_install:
  script:
    - /usr/bin/client --selftest
    - printf "[config]\nserver = https://myserver.cern.ch\n" > /etc/client.conf
    - /usr/bin/client --debug login
```

You can add any necessary dependencies in the `before_script` section, but you will then have to
also include a command to install your RPMs. You can modify the `.test_install` job directly to
affect all OSes, or modify only the OS-specific jobs.

You can also add your own tests by adding another job that extends one of
the OS-specific jobs.

Here are a few examples:

### Adding a dependency for all OSes and tests

```yaml
.test_install:
  before_script:
    - yum install -y --nogpgcheck --enablerepo=cernonly oracle-instantclient-tnsnames.ora
    - yum install -y --nogpgcheck `ls koji/*${DIST}.noarch.rpm koji/*${DIST}.x86_64.rpm`
```

### Adding your own Koji tag's repo (for extra dependencies)

This makes use of the `KOJI_TAG` variable, or of the `KOJI_TAG_x` variables, should they
be defined. The first two lines of the script should be included without modifications.

```yaml
.test_install:
  before_script:
    - export _KOJITAG_OS="KOJI_TAG_${_KOJI_OS}"
    - export _KOJITAG=${!_KOJITAG_OS:-"${KOJI_TAG}${_KOJI_OS}"}
    - yum-config-manager --add-repo "http://linuxsoft.cern.ch/internal/repos/${_KOJITAG}-stable/\$basearch/os"
    - sed -i "s/\[.*\]/[${_KOJITAG}]\npriority=1/" /etc/yum.repos.d/linuxsoft.cern.ch*${_KOJITAG}*.repo
    - yum install -y --nogpgcheck `ls koji/*${DIST}.noarch.rpm koji/*${DIST}.x86_64.rpm`
```

### New job with extra tests

```yaml
test_nose7:
  extends: test_install7
  script:
    # These deps are only needed to run the unit tests
    - yum install --nogpgcheck -y python-nose python2-mock python-suds
    - cd t
    - nosetests

test_nose6:
  extends: test_install6
  before_script:
    - yum install -y python2-future
    - yum install -y --nogpgcheck `ls koji/*${DIST}.noarch.rpm koji/*${DIST}.x86_64.rpm`
  script:
    # These deps are only needed to run the unit tests
    - yum install --nogpgcheck -y python-nose python2-mock python-suds
    - cd t
    - nosetests
```

### Replacing a particular OS' test

You may not want to install all the RPMs that you've built:

```yaml
test_install7:
  before_script:
    - yum install -y --nogpgcheck koji/myrpm-client*.rpm
```

### Configuring rpmlint

You can create a custom `.rpmlint` configuration file to disable some tests:

```
addFilter("E: hardcoded-library-path")
addFilter("myrpm.spec:6: W: non-standard-group")
```

### Ignoring rpmlint

If you don't care about rpmlint results, you can turn it into a manual job
so it's not run automatically:

```yaml
.test_rpmlint:
  when: manual
```

You can also let it run, but not have it stop the pipeline if there are errors:

```yaml
.test_rpmlint:
  allow_failure: true
```

### Adding build dependencies

If your local builds (`srpm` and `rpm` stages) require dependencies from other repos,
such as from your Koji tag, you can add them for all OSes as follows:

```yaml
.rpm_deps:
  before_script:
    - export _KOJITAG_OS="KOJI_TAG_${_KOJI_OS}"
    - export _KOJITAG=${!_KOJITAG_OS:-"${KOJI_TAG}${_KOJI_OS}"}
    - yum-config-manager --add-repo "http://linuxsoft.cern.ch/internal/repos/${_KOJITAG}-stable/\$basearch/os"
    - sed -i "s/\[.*\]/[${_KOJITAG}]\npriority=1/" /etc/yum.repos.d/linuxsoft.cern.ch*${_KOJITAG}*.repo
    - yum-builddep -y *.spec
```

You can also add different dependencies per OS, like this:

```yaml
.rpm_deps8:
  before_script:
    - yum-config-manager --add-repo "http://linuxsoft.cern.ch/internal/repos/dbclients8-stable/\$basearch/os/"
    - yum install -y oracle-instantclient
    - yum-builddep -y *.spec

.rpm_deps7:
  before_script:
    - yum install -y --enablerepo=cernonly oracle-instantclient
    - yum-builddep -y *.spec
```

You may also perform conditional actions depending on the OS by using the `$_KOJI_OS` variable:

```yaml
.rpm_deps:
  before_script:
    - |
      if [[ "$_KOJI_OS" -eq 8 ]]; then
        yum-config-manager --add-repo "http://linuxsoft.cern.ch/internal/repos/dbclients8-stable/\$basearch/os/"
      fi
    - yum-builddep -y *.spec
```

Don't forget to include the `yum-builddep -y *.spec` line in your `before_script`, otherwise
no dependencies will actually be installed!

### Adding your own CI stages

There are some unused stages (`prebuild`, `prekoji`, `pretest`, `posttest`, `postkoji`,
`docker_prepare` and `docker_build`) which you can use to define your own custom jobs.

If these are insufficient, or you wish to add/modify/reorder the stages of the CI process,
you will have to redefine the ones included in [rpm-ci.yml](https://gitlab.cern.ch/linuxsupport/rpmci/blob/master/rpm-ci.yml)
and add your own in your own `.gitlab-ci.yml` file. Note, however, that there are
`dependencies` and `needs` clauses between some jobs that you may need to modify.


## Tips and Tricks

### Debugging builds locally

The best way to debug your builds locally is to run them within the same Docker images RPMCI uses. You can do something like this:

```bash
~ > git clone https://:@gitlab.cern.ch:8443/linuxsupport/myrpm.git
...
~ > docker run --rm -it -v ~/myrpm:/build gitlab-registry.cern.ch/linuxsupport/rpmci/builder-c8:latest
...
[root@99369f9e7a22 /]# cd /build/
[root@99369f9e7a22 build]# yum-builddep -y *.spec
...
[root@99369f9e7a22 build]# make rpm
...
```

If it builds here, it will work in Gitlab CI and it has a high chance of success in Koji.

### CI jobs timing out

Gitlab CI's default timeout for jobs is 1 hour. If one of your jobs takes longer than that,
it will be terminated and the pipeline won't continue. You can change this timeout for your
project in `Settings` -> `CI / CD` -> `General pipelines` -> `Timeout`.

### Building Go binaries on CC7

If you're building golang binaries on CC7 and you want the EPEL golang, you'll have to do the following:

```yaml
build_srpm7:
  before_script:
    - yum-builddep -y --disableplugin=protectbase,priorities *.spec

build_rpm7:
  before_script:
    - yum-builddep -y --disableplugin=protectbase,priorities *.spec
```

Your spec file should also contain a `BuildRequires: golang`.

Lastly, the `git` version available on CC7 can produce errors like the following while fetching Go dependencies:

```
fatal: git fetch-pack: expected shallow list
```

One solution to this issue is to use `rh-git29` software collection. To do that, you should define a `before_script` similar to the following one:

```
  before_script:
    (...)
    - yum-config-manager --add-repo "http://linuxsoft.cern.ch/cern/centos/7/sclo/x86_64/rh"
    - yum install -y --nogpgcheck rh-git29
    - export PATH=/opt/rh/rh-git29/root/usr/bin:$PATH
    - export LD_LIBRARY_PATH=/opt/rh/httpd24/root/usr/lib64:$LD_LIBRARY_PATH
    - export MANPATH=/opt/rh/rh-git29/root/usr/share/man:$MANPATH
    - export PERL5LIB=/opt/rh/rh-git29/root/usr/share/perl5/vendor_perl
    - yum-builddep -y *.spec
```

This could be simplified by calling `source /bin/scl_source enable rh-git29`, but unfortunately it seems to fail.

### Building Maven packages

Normal Maven builds will not work in Koji because Koji builders don't
have internet access. The solution to this is to cache all the dependencies
before sending the job to Koji.

Edit your `Makefile` to download all the dependencies to the `.m2` directory and add it to the package:

```Makefile
sources:
    mvn -Dmaven.repo.local="$(PWD)/.m2" surefire:help dependency:go-offline
    tar -zcvf $(TARFILE) --transform 's,^,$(SPECFILE_NAME)-$(SPECFILE_VERSION)/,' src .m2 pom.xml
```

Edit your spec file to add `maven-local` and build from `.m2`:

```
...
BuildRequires:  maven-local
...

%setup -q
%mvn_file : %{name}/%{name} %{name}

%build
%mvn_build -f -j -- -Dmaven.repo.local=".m2"

%install
%mvn_install
...
```

You can find a full example of a Maven build [here](https://gitlab.cern.ch/ComputerSecurity/cert-flume-extra/-/tree/master).

### VCS tag in Spec files

On CC7 and C8, rpmci will automatically add a `VCS` tag to your spec file that
points to your gitlab repo at the commit hash the RPM is built from. This gives
you another way of tracing the source code that was used to build your package.

You can see the value of this tag on a built package like this:

```bash
~ > rpm -qp --qf "%{VCS}\n" ~/myrpm-1.1-6.el8.x86_64.rpm
https://gitlab.cern.ch/linuxsupport/myrpm/-/tree/60f909134d2ec5895cec23eabc75084502d091d7
```

By default, the `VCS` tag is `"${CI_PROJECT_URL}/-/tree/${CI_COMMIT_SHA}"`, but you can
customize this by supplying your own format in the `_VCS_TAG` variable.

The `VCS` tag won't be added if one already exists in the spec file. You can also
stop adding this tag altogether by setting the variable `ADD_VCS_TAG` to `False`.

## Getting Help

If you run into issues on how to use RPMCI, you can contact us on the [Linux Mattermost channel](https://mattermost.web.cern.ch/it-dep/channels/linux)
or by opening a [ServiceNOW ticket](https://cern.service-now.com/service-portal/report-ticket.do?name=incident&fe=IT-RPM).
