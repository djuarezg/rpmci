---
variables:
  _KOJI_PROFILE: 'koji'
  ADD_VCS_TAG: 'True'
  _VCS_TAG: "${CI_PROJECT_URL}/-/tree/${CI_COMMIT_SHA}"
  IMAGE_KOJI: 'gitlab-registry.cern.ch/linuxsupport/rpmci/kojicli'
  IMAGE_S8: 'gitlab-registry.cern.ch/linuxsupport/rpmci/builder-c8'
  IMAGE_8: 'gitlab-registry.cern.ch/linuxsupport/rpmci/builder-c8'
  IMAGE_7: 'gitlab-registry.cern.ch/linuxsupport/rpmci/builder-cc7'
  IMAGE_6: 'gitlab-registry.cern.ch/linuxsupport/rpmci/builder-slc6'
  BUILD_S8: 'False'
  BUILD_8: 'False'
  BUILD_7: 'False'
  BUILD_6: 'False'
  DIST_S8: '.el8s'
  DIST_8: '.el8'
  DIST_7: '.el7'
  DIST_6: '.slc6'
  KOJI_TAG_S8: ''
  KOJI_TAG_8: ''
  KOJI_TAG_7: ''
  KOJI_TAG_6: ''

include:
  - 'https://gitlab.cern.ch/linuxsupport/rpmci/raw/master/dist_8s.yml'
  - 'https://gitlab.cern.ch/linuxsupport/rpmci/raw/master/dist_8.yml'
  - 'https://gitlab.cern.ch/linuxsupport/rpmci/raw/master/dist_7.yml'
  - 'https://gitlab.cern.ch/linuxsupport/rpmci/raw/master/dist_6.yml'

stages:
  - prebuild
  - srpm
  - rpm
  - lint
  - prekoji
  - koji_scratch
  - pretest
  - test
  - posttest
  - koji_build
  - postkoji
  - docker_prepare
  - docker_build
  - deploy_qa
  - deploy_stable

.add_vcs_tag: &add_vcs_tag
  - |
    if [[ "$ADD_VCS_TAG" == 'True' && $_KOJI_OS != 6 ]]; then
      for SPEC in *.spec; do
        if ! grep -q "VCS:" "$SPEC"; then
          echo -e "\e[32mAdding VCS tag to spec file: ${_VCS_TAG}\e[0m"
          sed -i "1iVCS: ${_VCS_TAG}" "$SPEC"
        fi
      done
    fi

.rpm_deps:
  image: $IMAGE
  interruptible: true
  before_script:
    - yum-builddep -y *.spec
  dependencies: []

.build_srpm:
  extends: .rpm_deps
  stage: srpm
  script:
    - *add_vcs_tag
    - export MAKEFILE=`[ -e Makefile.koji ] && echo "Makefile.koji" || echo "Makefile"`
    - make -f $MAKEFILE srpm
    - COUNT=`ls -l build/SRPMS/*${DIST}.src.rpm 2>/dev/null | wc -l`
    - |
      if [[ $COUNT -eq 0 ]]; then
        echo -e "\e[1m\e[31mNo source RPMs found in build/SRPMS/, please check your Makefile and your .spec\e[0m"
        exit 1
      fi
  artifacts:
    expose_as: 'Source RPM'
    paths:
      - build/SRPMS/
    expire_in: 1 month

.build_rpm:
  extends: .rpm_deps
  stage: rpm
  script:
    - *add_vcs_tag
    - export MAKEFILE=`[ -e Makefile.koji ] && echo "Makefile.koji" || echo "Makefile"`
    - make -f $MAKEFILE rpm
    - |
      if [[ ! -d build/RPMS/ ]]; then
        echo -e "\e[1m\e[31mbuild/SRPMS/ not found, please check your Makefile and your .spec\e[0m"
        exit 1
      fi
  artifacts:
    paths:
      - build/RPMS/
    expire_in: 1 month

.test_rpmlint:
  image: $IMAGE
  stage: lint
  interruptible: true
  script:
    - |
      if [[ -e .rpmlint ]]; then
        echo -e "\e[32mUsing custom rpmlint configuration from .rpmlint\e[0m"
        CONFIG="-f .rpmlint"
      else
        echo -e "\e[32mUsing default rpmlint configuration. Create a .rpmlint file to override settings\e[0m"
      fi
    - rpmlint -i $CONFIG *.spec
    - rpmlint -i $CONFIG build/

.test_install:
  image: $IMAGE
  stage: test
  interruptible: true
  before_script:
    - export _KOJITAG_OS="KOJI_TAG_${_KOJI_OS}"
    - export _KOJITAG=${!_KOJITAG_OS:-"${KOJI_TAG}${_KOJI_OS}"}
    - yum install -y --nogpgcheck `ls koji/*${DIST}.noarch.rpm koji/*${DIST}.x86_64.rpm`
  script:
    - echo "Test me!"

.koji_deps:
  image: $IMAGE_KOJI
  interruptible: true
  before_script:
    - |
      if [[ -z "$KOJICI_USER" ]]; then
        echo -e "\e[1m\e[31mVariable KOJICI_USER not defined\e[0m"
        exit 1
      fi
    - |
      if [[ -z "$KOJICI_PWD" ]]; then
        echo -e "\e[1m\e[31mVariable KOJICI_PWD not defined\e[0m"
        exit 1
      fi
    - mkdir koji
    - export _KOJITAG_OS="KOJI_TAG_${_KOJI_OS}"
    - export _KOJITAG=${!_KOJITAG_OS:-"${KOJI_TAG}${_KOJI_OS}"}
  dependencies: []
  variables:
    GIT_STRATEGY: none

.koji_scratch:
  extends: .koji_deps
  stage: koji_scratch
  script:
    - |
      for SRPM in build/SRPMS/*${DIST}.src.rpm; do
        export BUILDNAME=$(rpm -qp --qf "%{n}-%{v}-%{r}\n" ${SRPM})
        echo -e "\e[32mBuild name of ${SRPM} is \"${BUILDNAME}\"\e[0m"
        echo -e "\e[1m\e[32mkoji -p ${_KOJI_PROFILE} build --wait --scratch ${_KOJITAG} ${SRPM}\e[0m"
        koji -p ${_KOJI_PROFILE} build --wait --scratch ${_KOJITAG} ${SRPM} | tee taskid
        export TASKID=$(sed -z -e 's/.*Created task:\ \([0-9]\+\).*/\1/g' taskid)
        cd koji
        echo -e "\e[1m\e[32mkoji -p ${_KOJI_PROFILE} download-task --noprogress $TASKID\e[0m"
        koji -p ${_KOJI_PROFILE} download-task --noprogress $TASKID
        cd ~-
        export LOCAL_VERSION=$(rpm -qp --qf "%{v}-%{r}\n" ${SRPM})
        export KOJI_VERSION=$(rpm -qp --qf "%{v}-%{r}\n" koji/${BUILDNAME%${DIST}}*.src.rpm)
        echo -e "\e[32mLocal version is ${LOCAL_VERSION}, Koji version is ${KOJI_VERSION}\e[0m"
        if [[ "$KOJI_VERSION" != "$LOCAL_VERSION" ]]; then
          echo -e "\e[1m\e[31mThe version created locally does not match the one created by Koji.\e[0m"
          echo -e "\e[1m\e[31mMake sure you specify the DIST_* variable correctly in your CI configuration.\e[0m"
          exit 1
        fi
      done
  artifacts:
    expose_as: 'Koji scratch RPMs'
    paths:
      - koji/
    expire_in: 1 month

.koji_build:
  extends: .koji_deps
  stage: koji_build
  only:
    refs:
      - tags
  script:
    - |
      for SRPM in build/SRPMS/*${DIST}.src.rpm; do
        export PKGNAME=$(rpm -qp --qf "%{n}\n" ${SRPM})
        PKGEXISTS=$(koji -p ${_KOJI_PROFILE} list-pkgs --quiet --noinherit --tag="${_KOJITAG}-testing" --package="${PKGNAME}" > /dev/null 2>&1; echo $?)
        if [ $PKGEXISTS -eq 1 ]; then
          echo -e "\e[1m\e[31mPackage ${PKGNAME} has not been added to the tag ${_KOJITAG}.\e[0m"
          echo "You can add it by running something like this:"
          echo "  for i in testing qa stable; do koji add-pkg --owner=$KOJICI_USER ${_KOJITAG}-\$i ${PKGNAME}; done"
          exit 1
        fi
        export BUILDNAME=$(rpm -qp --qf "%{n}-%{v}-%{r}\n" ${SRPM})
        echo -e "\e[32mBuild name of ${SRPM} is \"${BUILDNAME}\"\e[0m"
        BUILDEXISTS=$(koji -p ${_KOJI_PROFILE} list-builds --quiet --buildid="${BUILDNAME}" 2>&1 | grep -q COMPLETE; echo $?)
        if [ $BUILDEXISTS -eq 0 ]; then
          echo -e "\e[1m\e[31mBuild ${BUILDNAME} already exists in Koji, please update the version number\e[0m"
          exit 1
        fi
        echo -e "\e[1m\e[32mkoji -p ${_KOJI_PROFILE} build --wait ${_KOJITAG} ${SRPM}\e[0m"
        koji -p ${_KOJI_PROFILE} build --wait ${_KOJITAG} ${SRPM}
        cd koji
        echo -e "\e[1m\e[32mkoji -p ${_KOJI_PROFILE} download-build --noprogress --debuginfo ${BUILDNAME}\e[0m"
        koji -p ${_KOJI_PROFILE} download-build --noprogress --debuginfo ${BUILDNAME}
        cd ~-
      done
  artifacts:
    expose_as: 'Koji RPMs'
    paths:
      - koji/
    expire_in: 1 month

.tag_qa:
  extends: .koji_deps
  stage: deploy_qa
  when: manual
  only:
    refs:
      - tags
  script:
    - |
      for SRPM in build/SRPMS/*${DIST}.src.rpm; do
        export BUILDNAME=$(rpm -qp --qf "%{n}-%{v}-%{r}\n" ${SRPM})
        echo -e "\e[32mBuild name of ${SRPM} is \"${BUILDNAME}\"\e[0m"
        echo -e "\e[1m\e[32mkoji -p ${_KOJI_PROFILE} tag-build ${_KOJITAG}-qa ${BUILDNAME}\e[0m"
        koji -p ${_KOJI_PROFILE} tag-build ${_KOJITAG}-qa ${BUILDNAME}
      done
  allow_failure: false

.tag_stable:
  extends: .koji_deps
  stage: deploy_stable
  when: manual
  only:
    refs:
      - tags
  script:
    - |
      for SRPM in build/SRPMS/*${DIST}.src.rpm; do
        export BUILDNAME=$(rpm -qp --qf "%{n}-%{v}-%{r}\n" ${SRPM})
        echo -e "\e[32mBuild name of ${SRPM} is \"${BUILDNAME}\"\e[0m"
        echo -e "\e[1m\e[32mkoji -p ${_KOJI_PROFILE} tag-build ${_KOJITAG}-stable ${BUILDNAME}\e[0m"
        koji -p ${_KOJI_PROFILE} tag-build ${_KOJITAG}-stable ${BUILDNAME}
      done
